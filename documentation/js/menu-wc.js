'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">daily-planner-app documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' : 'data-target="#xs-controllers-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' :
                                            'id="xs-controllers-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' : 'data-target="#xs-injectables-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' :
                                        'id="xs-injectables-links-module-AppModule-b080914ec1f6ef1b8c83d1ff2cc9a390838c3543cc67356f80d4c3ed6b418749674b10fc736a6c5e70e6c340d2ee4c51add6d9e348ab6ce0231a3e49f206fe77"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/createTaskModule.html" data-type="entity-link" >createTaskModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' : 'data-target="#xs-controllers-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' :
                                            'id="xs-controllers-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' }>
                                            <li class="link">
                                                <a href="controllers/CreateTaskController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CreateTaskController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' : 'data-target="#xs-injectables-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' :
                                        'id="xs-injectables-links-module-createTaskModule-05f894ef99cb9b9a067f61bc480b43d7f87d04a776d26f83872288e1f24bb1794912476e30551523b865d19bae482d8573d901795d8fb49729992584210b6588"' }>
                                        <li class="link">
                                            <a href="injectables/CreateTaskService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CreateTaskService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/RecurringTaskModule.html" data-type="entity-link" >RecurringTaskModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' : 'data-target="#xs-controllers-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' :
                                            'id="xs-controllers-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' }>
                                            <li class="link">
                                                <a href="controllers/RecurringTaskController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RecurringTaskController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' : 'data-target="#xs-injectables-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' :
                                        'id="xs-injectables-links-module-RecurringTaskModule-18c758bb6a9e89e1c2cab8ac7853f211bd2649615f94afdd2b5b7c6a7eb62bd3cc02e955f4780ee6184a02621686cccdb5c5664aaaf5616de50821074e883573"' }>
                                        <li class="link">
                                            <a href="injectables/RecurringTaskService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RecurringTaskService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SubTaskModule.html" data-type="entity-link" >SubTaskModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' : 'data-target="#xs-controllers-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' :
                                            'id="xs-controllers-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' }>
                                            <li class="link">
                                                <a href="controllers/SubTaskController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SubTaskController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' : 'data-target="#xs-injectables-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' :
                                        'id="xs-injectables-links-module-SubTaskModule-9c4ba9477c3cf53d4c9c95305e06836ae2f6d5bb49395ff47b2931abe02db01f8952dc772a20cfbddffd0cbfb3afb44e29474e6fd5aade3eb9972edab1f08f9e"' }>
                                        <li class="link">
                                            <a href="injectables/SubTaskService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SubTaskService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' : 'data-target="#xs-controllers-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' :
                                            'id="xs-controllers-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' : 'data-target="#xs-injectables-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' :
                                        'id="xs-injectables-links-module-UserModule-19733bdc5bd9cb49566289c1eb1d5d050e4e1caaf5c9c3c9d74649782092cb2ddc0a36087e8d54369024b706371d883764b180c40e25e55ea311c624b845c726"' }>
                                        <li class="link">
                                            <a href="injectables/JwtToken.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtToken</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/CreateTaskController.html" data-type="entity-link" >CreateTaskController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/RecurringTaskController.html" data-type="entity-link" >RecurringTaskController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/SubTaskController.html" data-type="entity-link" >SubTaskController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/CreatingTask.html" data-type="entity-link" >CreatingTask</a>
                                </li>
                                <li class="link">
                                    <a href="entities/RecurringTasks.html" data-type="entity-link" >RecurringTasks</a>
                                </li>
                                <li class="link">
                                    <a href="entities/SubTask.html" data-type="entity-link" >SubTask</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Maintainance.html" data-type="entity-link" >Maintainance</a>
                            </li>
                            <li class="link">
                                <a href="classes/notFoundException.html" data-type="entity-link" >notFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/unauthorizedException.html" data-type="entity-link" >unauthorizedException</a>
                            </li>
                            <li class="link">
                                <a href="classes/userNotFoundException.html" data-type="entity-link" >userNotFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/WrongCredentialsException.html" data-type="entity-link" >WrongCredentialsException</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CreateTaskService.html" data-type="entity-link" >CreateTaskService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtToken.html" data-type="entity-link" >JwtToken</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RecurringTaskService.html" data-type="entity-link" >RecurringTaskService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SubTaskService.html" data-type="entity-link" >SubTaskService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link" >RolesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});