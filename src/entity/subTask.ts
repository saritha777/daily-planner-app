import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { CreatingTask } from './creatingTask.entity';
import { Maintainance } from './maintainance';

@Entity()
export class SubTask extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @ApiProperty()
  @Column()
  taskName: string;

  @IsString()
  @ApiProperty()
  @Column()
  description: string;

  @IsString()
  @ApiProperty()
  @Column()
  date: string;

  @IsString()
  @ApiProperty()
  @Column()
  status: string;

  @ManyToOne(() => CreatingTask, (task) => task.subtask, {
    cascade: true
  })
  @JoinColumn()
  task: CreatingTask;
}
