import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Maintainance } from './maintainance';
import { SubTask } from './subTask';
import { User } from './user.entity';

@Entity()
export class CreatingTask extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @ApiProperty()
  @Column()
  taskName: string;

  @IsString()
  @ApiProperty()
  @Column()
  description: string;

  @IsString()
  @ApiProperty()
  @Column()
  date: string;

  @IsString()
  @ApiProperty()
  @Column()
  status: string;

  @ManyToOne(() => User, (user) => user.createtask)
  user: User;

  @OneToMany(() => SubTask, (subtask) => subtask.task, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  subtask: SubTask[];

  addSubTask(subtask: SubTask) {
    if (this.subtask == null) {
      this.subtask = new Array<SubTask>();
    }
    this.subtask.push(subtask);
    return subtask;
  }
}
