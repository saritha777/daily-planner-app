import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { CreatingTask } from './creatingTask.entity';
import { UserLogin } from './login.entity';
import { RecurringTasks } from './recurringTask.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @ApiProperty()
  @Column()
  name: string;

  @IsString()
  @ApiProperty()
  @Column()
  password: string;

  @IsString()
  @ApiProperty()
  @Column({ unique: true })
  email: string;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true
  })
  login: UserLogin;

  @OneToMany(() => CreatingTask, (createtask) => createtask.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  createtask: CreatingTask[];

  addTask(createtask: CreatingTask) {
    if (this.createtask == null) {
      this.createtask = new Array<CreatingTask>();
    }
    this.createtask.push(createtask);
    return createtask;
  }

  @OneToMany(() => RecurringTasks, (recurringtasks) => recurringtasks.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  recurringtasks: RecurringTasks[];
  addrecurringtask(recurringtasks: RecurringTasks) {
    if (this.recurringtasks == null) {
      this.recurringtasks = new Array<RecurringTasks>();
    }
    this.recurringtasks.push(recurringtasks);
    return recurringtasks;
  }
}
