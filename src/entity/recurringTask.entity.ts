import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Maintainance } from './maintainance';
import { User } from './user.entity';

@Entity()
export class RecurringTasks extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @ApiProperty()
  @Column()
  startDate: string;

  @IsString()
  @ApiProperty()
  @Column()
  endDate: string;

  @IsString()
  @ApiProperty()
  @Column()
  taskName: string;

  @IsString()
  @ApiProperty()
  @Column()
  time: string;

  @IsString()
  @ApiProperty()
  @Column()
  status: string;

  @IsBoolean()
  @ApiProperty()
  @Column()
  occurence: boolean;

  @ManyToOne(() => User, (user) => user.recurringtasks, {
    cascade: true
  })
  @JoinColumn()
  user: User;
}
