import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column } from 'typeorm';

export class Maintainance {
  @IsString()
  @ApiProperty()
  @Column()
  isActive: boolean;

  @IsString()
  @ApiProperty()
  @Column()
  createdBy: string;

  @IsString()
  @ApiProperty()
  @Column()
  createdTime: string;

  @IsString()
  @ApiProperty()
  @Column()
  updatedBy: string;

  @IsString()
  @ApiProperty()
  @Column()
  updatedTime: string;
}
