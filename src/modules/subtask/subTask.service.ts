import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SubTask } from '../../entity/subTask';
import { Repository } from 'typeorm';
import { CreatingTask } from '../../entity/creatingTask.entity';

@Injectable()
export class SubTaskService {
  constructor(
    @InjectRepository(SubTask)
    private subTaskRepository: Repository<SubTask>,
    @InjectRepository(CreatingTask)
    private createtaskRepository: Repository<CreatingTask>
  ) {}
  async addSubTask(taskName: string, data: SubTask) {
    const info = await this.createtaskRepository.findOne({
      taskName: taskName
    });
    if (!info) {
      return "task doesn't exists";
    } else {
      const subtaskdata = new SubTask();
      subtaskdata.createdBy = data.createdBy;
      subtaskdata.createdTime = data.createdTime;
      subtaskdata.date = data.date;
      subtaskdata.description = data.description;
      subtaskdata.isActive = data.isActive;
      subtaskdata.status = data.status;
      subtaskdata.taskName = data.taskName;
      subtaskdata.updatedBy = data.updatedBy;
      subtaskdata.updatedTime = data.updatedTime;
      let task = new CreatingTask();
      task = info;
      subtaskdata.task = task;
      return await this.subTaskRepository.save(subtaskdata);
    }
  }
  async getAllSubTasks(): Promise<SubTask[]> {
    return await this.subTaskRepository.find();
  }
  async getSubTaskByTaskName(taskName: string): Promise<SubTask[]> {
    return await this.subTaskRepository.find({ taskName: taskName });
  }
  async getTaskByDate(date: string): Promise<SubTask[]> {
    return await this.subTaskRepository.find({ date: date });
  }

  async getTaskByStatus(status: string): Promise<SubTask[]> {
    return await this.subTaskRepository.find({ status: status });
  }

  async updateCreatedTask(id: number, subtask: SubTask) {
    const subtaskdata = new SubTask();
    subtaskdata.createdBy = subtask.createdBy;
    subtaskdata.createdTime = subtask.createdTime;
    subtaskdata.date = subtask.date;
    subtaskdata.description = subtask.description;
    subtaskdata.isActive = subtask.isActive;
    subtaskdata.status = subtask.status;
    subtaskdata.taskName = subtask.taskName;
    subtaskdata.updatedBy = subtask.updatedBy;
    subtaskdata.updatedTime = subtask.updatedTime;

    await this.subTaskRepository.update({ id: id }, subtask);
    return 'Updated tasks successfully';
  }
  async deleteSubTasks(id: number) {
    await this.subTaskRepository.delete({ id });
    return ' Deleted tasks successfully';
  }
}
