import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SubTask } from '../../entity/subTask';
import { SubTaskService } from './subTask.service';

@ApiTags('subTask')
@Controller('/subTask')
export class SubTaskController {
  constructor(private readonly subTaskService: SubTaskService) {}

  @Post('/:taskName')
  async addSubTask(
    @Param('taskName') taskName: string,
    @Body() subTask: SubTask
  ) {
    return await this.subTaskService.addSubTask(taskName, subTask);
  }
  @Get()
  async getAllSubTasks(): Promise<SubTask[]> {
    return await this.subTaskService.getAllSubTasks();
  }
  @Get('/taskName/:taskName')
  async getSubTaskByTaskName(@Param('taskName') taskName: string) {
    return await this.subTaskService.getSubTaskByTaskName(taskName);
  }
  @Get('date/:date')
  getTaskByDate(@Param('date') date: string) {
    return this.subTaskService.getTaskByDate(date);
  }

  @Get('taskstatus/:status')
  getTaskByStatus(@Param('status') status: string) {
    return this.subTaskService.getTaskByStatus(status);
  }
  @Put('/:id')
  async updateSubTask(@Param('id') id: number, @Body() createtask: SubTask) {
    return await this.subTaskService.updateCreatedTask(id, createtask);
  }
  @Delete('/:id')
  deleteSubTasks(@Param('id') id: number) {
    return this.subTaskService.deleteSubTasks(id);
  }
}
