import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreatingTask } from '../../entity/creatingTask.entity';
import { SubTask } from '../../entity/subTask';
import { SubTaskController } from './subTask.controller';
import { SubTaskService } from './subTask.service';

@Module({
  imports: [TypeOrmModule.forFeature([SubTask, CreatingTask])],
  controllers: [SubTaskController],
  providers: [SubTaskService]
})
export class SubTaskModule {}
