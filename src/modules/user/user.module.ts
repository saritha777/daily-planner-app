import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtToken } from '../../common/provides/jwtservice';
import { CreatingTask } from '../../entity/creatingTask.entity';
import { UserLogin } from '../../entity/login.entity';
import { RecurringTasks } from '../../entity/recurringTask.entity';
import { User } from '../../entity/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLogin, CreatingTask, RecurringTasks]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [UserController],
  providers: [UserService, JwtToken]
})
export class UserModule {}
