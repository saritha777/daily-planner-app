import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserLogin } from '../../entity/login.entity';
import { User } from '../../entity/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Request, Response } from 'express';
import { JwtToken } from '../../common/provides/jwtservice';

/**
 * user service having all business logics realted to user
 */
@Injectable()
export class UserService {
  jwtService: any;

  /**
   * injecting the all repository
   * @param userRepository user repository having all user info
   * @param loginRepository login repository having all login info
   * @param jwtToken jwt token having info about token
   */
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    private jwtToken: JwtToken
  ) {}

  /**
   * add user method
   * @param data taking user data
   * @returns user details
   */
  async addUser(data: User): Promise<User> {
    const pass = await bcrypt.hash(data.password, 10);
    const userData = new User();
    const loginData = new UserLogin();
    userData.name = data.name;
    userData.email = loginData.email = data.email;
    userData.password = loginData.password = pass;
    userData.login = loginData;
    return this.userRepository.save(userData);
  }

  /**
   * login method
   * @param data taking userlogin data
   * @param response taking response
   * @param req requesting for a cookie
   * @returns jwt
   */
  async login(data: UserLogin, response: Response, req: Request) {
    const user = await this.loginRepository.findOne({ email: data.email });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtToken.generateToken(user);
    response.cookie('jwt', jwt, { httpOnly: true });
    console.log('printing');
    const cookie = req.cookies['jwt'];
    const c = await this.jwtToken.verifyToken(cookie);
    // console.log(c);
    return c;
  }

  /**
   * method for getting all users
   * @returns all users data
   */
  async getAll(): Promise<User[]> {
    return this.userRepository.find();
  }
}
