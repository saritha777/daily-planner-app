import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { UserLogin } from '../../entity/login.entity';
import { User } from '../../entity/user.entity';
import { UserService } from './user.service';
import { Request, Response } from 'express';
/**
 * user controller
 * having all info about user
 */
@Controller()
export class UserController {
  /**
   * user service
   * @param userService injecting user service
   */
  constructor(private readonly userService: UserService) {}

  /**
   * post method for posting the user data
   * @param user taking user data as input
   * @returns User data
   */
  @Post()
  async addUser(@Body() user: User): Promise<User> {
    return await this.userService.addUser(user);
  }

  /**
   * method for login
   * @param login taking login info from the user
   * @param response taking reponse
   * @param request request for object
   * @returns login info user logedin or not
   */
  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response,
    @Req() request: Request
  ) {
    return await this.userService.login(login, response, request);
  }

  /**
   * method for getting all users
   * @returns all users
   */
  @Get()
  async getAll() {
    return this.userService.getAll();
  }
}
