import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SubTask } from '../../entity/subTask';
import { CreatingTask } from '../../entity/creatingTask.entity';
import { CreateTaskService } from './task.service';

/**
 * Create task controller having info about create task
 */
@ApiTags('create task')
@Controller('/createTasks')
export class CreateTaskController {
  /**
   * create task service obj
   * @param createTaskService injecting create task service
   */
  constructor(private readonly createTaskService: CreateTaskService) {}

  /**
   * post method for creating task
   * @param email taking email as input from the user data
   * @param createtask taking create task deatails
   * @returns create task
   */
  @Post('/addtask/:email')
  async addCreateTask(
    @Param('email') email: string,
    @Body() createtask: CreatingTask
  ) {
    return await this.createTaskService.addCreateTask(email, createtask);
  }

  /**
   * get emthod for create tasks
   * @returns all created task details
   */
  @Get()
  async getAllTasks(): Promise<CreatingTask[]> {
    return await this.createTaskService.getAllTasks();
  }

  /**
   * get tasks by date method
   * @param date taking date
   * @returns all related to that date
   */
  @Get('/date/:date')
  async getTaskByDate(@Param('date') date: string) {
    return await this.createTaskService.getTaskByDate(date);
  }

  /**
   * get method based on status
   * @param status taking status as input
   * @returns all details based on status
   */
  @Get('/status/:status')
  async getTaskByStatus(@Param('status') status: string) {
    return await this.createTaskService.getTaskByStatus(status);
  }

  /**
   * update method for create task
   * @param id taking id for update
   * @param createtask taking
   * @returns
   */
  @Put('/:id')
  async updateCreatedTask(
    @Param('id') id: number,
    @Body() createtask: CreatingTask
  ) {
    return await this.createTaskService.updateCreatedTask(id, createtask);
  }

  /**
   * delete method
   * @param id taking id to delete
   * @returns whether the data is deleted or not
   */
  @Delete('/:id')
  deleteTasks(@Param('id') id: number) {
    return this.createTaskService.deleteTasks(id);
  }
}
