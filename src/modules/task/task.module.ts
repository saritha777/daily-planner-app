import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubTask } from '../../entity/subTask';
import { CreatingTask } from '../../entity/creatingTask.entity';
import { CreateTaskController } from './task.controller';
import { CreateTaskService } from './task.service';
import { User } from '../../entity/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CreatingTask, SubTask, User])],
  controllers: [CreateTaskController],
  providers: [CreateTaskService]
})
export class createTaskModule {}
