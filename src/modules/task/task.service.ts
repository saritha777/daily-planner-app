import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatingTask } from '../../entity/creatingTask.entity';
import { Repository } from 'typeorm';
import { SubTask } from '../../entity/subTask';
import { User } from '../../entity/user.entity';

/**
 * create task service
 */
@Injectable()
export class CreateTaskService {
  /**
   * for injecting repository
   * @param createtaskRepository create task repository all user info
   * @param userRepository user task repository having all user info
   */
  constructor(
    @InjectRepository(CreatingTask)
    private createtaskRepository: Repository<CreatingTask>,
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  /**
   * add create task method
   * @param email taking email as input
   * @param createtask taking create task details
   * @returns create task details
   */
  async addCreateTask(email: string, createtask: CreatingTask) {
    const user = await this.userRepository.findOne({
      email: email
    });
    if (user) {
      const info = await this.createtaskRepository.findOne({
        taskName: createtask.taskName
      });
      if (!info) {
        const taskdata = new CreatingTask();
        taskdata.taskName = createtask.taskName;
        taskdata.description = createtask.description;
        taskdata.date = createtask.date;
        taskdata.isActive = createtask.isActive;
        taskdata.status = createtask.status;
        taskdata.createdTime = createtask.createdTime;
        taskdata.updatedBy = createtask.updatedBy;
        taskdata.updatedTime = createtask.updatedTime;
        taskdata.createdBy = createtask.createdBy;
        taskdata.user = user;
        return await this.createtaskRepository.save(taskdata);
      } else {
        return 'task already exists';
      }
    } else {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
  }

  /**
   * get method for all
   * @returns all task details
   */
  async getAllTasks(): Promise<CreatingTask[]> {
    return await this.createtaskRepository.find();
  }

  /**
   * get method for getting tasks based on date
   * @param date taking date as input
   * @returns task details based on date
   */
  async getTaskByDate(date: string): Promise<CreatingTask[]> {
    return await this.createtaskRepository.find({ date: date });
  }

  /**
   * get method for getting tasks based on status
   * @param status taking status as input
   * @returns tasks based on status
   */
  async getTaskByStatus(status: string): Promise<CreatingTask[]> {
    return await this.createtaskRepository.find({ status: status });
  }

  /**
   * update method
   * @param id taking id for update
   * @param createtask taking create task details
   * @returns updated information
   */
  async updateCreatedTask(id: number, createtask: CreatingTask) {
    const taskdata = new CreatingTask();
    taskdata.taskName = createtask.taskName;
    taskdata.description = createtask.description;
    taskdata.date = createtask.date;
    taskdata.isActive = createtask.isActive;
    taskdata.status = createtask.status;
    taskdata.createdTime = createtask.createdTime;
    taskdata.updatedBy = createtask.updatedBy;
    taskdata.updatedTime = createtask.updatedTime;
    taskdata.createdBy = createtask.createdBy;
    await this.createtaskRepository.update({ id: id }, createtask);
    return 'Updated tasks successfully';
  }

  /**
   * delete method
   * @param id taking id for delete
   * @returns delted info
   */
  async deleteTasks(id: number) {
    await this.createtaskRepository.delete({ id });
    return ' Deleted tasks successfully';
  }
}
