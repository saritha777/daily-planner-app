import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RecurringTasks } from '../../entity/recurringTask.entity';
import { RecurringTaskService } from './recurringTAsk.service';

@ApiTags('recurringTask')
@Controller('/recurringTask')
export class RecurringTaskController {
  constructor(private readonly recurringService: RecurringTaskService) {}
  @Post('/:email')
  async addRecurringTask(
    @Param('email') email: string,
    @Body() recurringtask: RecurringTasks
  ): Promise<RecurringTasks> {
    return await this.recurringService.addRecurringTask(email, recurringtask);
  }
  @Get()
  async getAllRecurringTasks(): Promise<RecurringTasks[]> {
    return await this.recurringService.getAllRecurringTasks();
  }
  @Get('startDate/:startDate')
  getTaskByDate(@Param('startDate') startDate: string) {
    return this.recurringService.getTaskByDate(startDate);
  }

  @Get('taskstatus/:status')
  getTaskByStatus(@Param('status') status: string) {
    return this.recurringService.getTaskByStatus(status);
  }
  @Put('/:id')
  async updateReccuringTask(
    @Param('id') id: number,
    @Body() recurringtask: RecurringTasks
  ) {
    return await this.recurringService.updateReccuringTask(id, recurringtask);
  }
  @Delete('/:id')
  deleteRecurringTasks(@Param('id') id: number) {
    return this.recurringService.deleteRecurringTasks(id);
  }
}
