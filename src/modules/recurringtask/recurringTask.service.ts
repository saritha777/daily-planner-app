import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RecurringTasks } from '../../entity/recurringTask.entity';
import { Repository } from 'typeorm';
import { User } from '../../entity/user.entity';

@Injectable()
export class RecurringTaskService {
  constructor(
    @InjectRepository(RecurringTasks)
    private recurringtasksRepository: Repository<RecurringTasks>,
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}
  async addRecurringTask(
    email: string,
    recurringtask: RecurringTasks
  ): Promise<RecurringTasks> {
    const userdata = await this.userRepository.findOne({
      email: email
    });
    if (userdata) {
      const recurringdata = new RecurringTasks();
      recurringdata.taskName = recurringtask.taskName;
      recurringdata.time = recurringtask.time;
      recurringdata.createdTime = recurringtask.createdTime;
      recurringdata.endDate = recurringtask.endDate;
      recurringdata.isActive = recurringtask.isActive;
      recurringdata.occurence = recurringtask.occurence;
      recurringdata.startDate = recurringtask.startDate;
      recurringdata.updatedBy = recurringtask.updatedBy;
      recurringdata.updatedTime = recurringtask.updatedTime;
      recurringdata.createdBy = recurringtask.createdBy;
      recurringdata.status = recurringtask.status;
      recurringdata.endDate = recurringtask.endDate;
      recurringdata.user = userdata;
      return await this.recurringtasksRepository.save(recurringdata);
    } else {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
  }
  async getAllRecurringTasks(): Promise<RecurringTasks[]> {
    return await this.recurringtasksRepository.find();
  }
  async getTaskByDate(startDate: string): Promise<RecurringTasks[]> {
    return await this.recurringtasksRepository.find({
      startDate: startDate
    });
  }

  async getTaskByStatus(status: string): Promise<RecurringTasks[]> {
    return await this.recurringtasksRepository.find({ status: status });
  }
  async updateReccuringTask(id: number, reccuringtask: RecurringTasks) {
    const recurringdata = new RecurringTasks();
    recurringdata.taskName = reccuringtask.taskName;
    recurringdata.time = reccuringtask.time;
    recurringdata.createdTime = reccuringtask.createdTime;
    recurringdata.endDate = reccuringtask.endDate;
    recurringdata.isActive = reccuringtask.isActive;
    recurringdata.occurence = reccuringtask.occurence;
    recurringdata.startDate = reccuringtask.startDate;
    recurringdata.updatedBy = reccuringtask.updatedBy;
    recurringdata.updatedTime = reccuringtask.updatedTime;
    recurringdata.createdBy = reccuringtask.createdBy;
    recurringdata.status = reccuringtask.status;
    recurringdata.endDate = reccuringtask.endDate;
    await this.recurringtasksRepository.update({ id: id }, reccuringtask);
    return 'updated successfully';
  }
  async deleteRecurringTasks(id: number) {
    await this.recurringtasksRepository.delete({ id });
    return ' Deleted tasks successfully';
  }
}
