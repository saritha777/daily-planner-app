import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../entity/user.entity';
import { RecurringTasks } from '../../entity/recurringTask.entity';
import { RecurringTaskController } from './recurringTask.controller';
import { RecurringTaskService } from './recurringTAsk.service';

@Module({
  imports: [TypeOrmModule.forFeature([RecurringTasks, User])],
  controllers: [RecurringTaskController],
  providers: [RecurringTaskService]
})
export class RecurringTaskModule {}
