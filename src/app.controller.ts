import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
/**
 * main controller
 */
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  /**
   * get method
   * @returns string from the app service
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
