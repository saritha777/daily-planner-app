import { SwaggerConfig } from './swagger.interface';

export const SWAGGER_CONFIG: SwaggerConfig = {
  title: 'daily- planner-app',
  description: 'daily traker',
  version: '1.0',
  tags: ['Templete']
};
