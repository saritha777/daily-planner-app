import { Injectable } from '@nestjs/common';

/**
 * main service
 */
@Injectable()
export class AppService {
  /**
   * method getHello
   * @returns string hello world
   */
  getHello(): string {
    return 'Hello World!';
  }
}
