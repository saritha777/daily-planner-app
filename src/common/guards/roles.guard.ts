import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { JwtToken } from '../provides/jwtservice';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private jwtToken: JwtToken
  ) {}

  canActivate(
    context: ExecutionContext
  ): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<string>('jwt', context.getHandler());
    console.log(roles);

    //  if (!roles) {
    //    return true;
    //  }

    if (!this.jwtToken.verifyToken(roles)) {
      return false;
    }

    return true;
  }
}
