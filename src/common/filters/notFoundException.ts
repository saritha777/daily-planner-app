import { HttpException, HttpStatus } from '@nestjs/common';

export class notFoundException extends HttpException {
  constructor(msg: string, httpstatus: HttpStatus) {
    super(msg, httpstatus);
  }
}
