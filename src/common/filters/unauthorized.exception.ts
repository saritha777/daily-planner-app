import { HttpException, HttpStatus } from '@nestjs/common';

export class unauthorizedException extends HttpException {
  constructor(msg: string, httpStatus: HttpStatus) {
    super(msg, httpStatus);
  }
}
