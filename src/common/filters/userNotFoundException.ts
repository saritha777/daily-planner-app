import { HttpException, HttpStatus } from '@nestjs/common';

export class userNotFoundException extends HttpException {
  constructor(msg: string, httpstatus: HttpStatus) {
    super(msg, httpstatus);
  }
}
