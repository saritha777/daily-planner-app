import { HttpException, HttpStatus } from '@nestjs/common';

export class WrongCredentialsException extends HttpException {
  constructor(msg: string, httpstatus: HttpStatus) {
    super(msg, httpstatus);
  }
}
